export interface Record {
  _id: string
  title: string
}
export interface Genre extends Record {
  _id: string
  title: string
  genre: string
  visible: boolean
}
export interface Category extends Record {
  _id: string
  title: string
  slug: string
  description: string
  titleSeo: string
  descSeo: string
  image: string
  visible: boolean
  createdAt?: Date
  updatedAt?: Date
  // stories?: Story[]
}
export interface Story extends Record {
  _id: string
  title: string
  slug: string
  description: string
  titleSeo: string
  descSeo: string
  image: string
  genres: Genre[] | null
  visible: boolean
  popular: boolean
  createdAt?: Date
  updatedAt?: Date
  // category?: string | null
  category?: Category | null
}
export interface Chap extends Record {
  _id: string
  title: string
  slug: string
  genre: Genre | null
  description: string
  image: string
  visible: boolean
  popular: boolean
  createdAt?: Date
  updatedAt?: Date
  // story?: string | null
  story?: Story | null
}
