import { Record } from "../../../interfaces/RecordEntities"
import { RecordEditProps } from "../../../interfaces/PagesProps"
import { RecordForm } from "./Form"

export const RecordEdit = <T extends Record>({
  FormFields,
  activeRecord,
  update,
  success
}: RecordEditProps<T>) =>
{
  console.log({ activeRecord })
  return (
    <>
      <h2>Edit</h2>
      <RecordForm
        FormFields={FormFields}
        activeRecord={activeRecord}
        submitAction={update}
        success={success}
      />
    </>
  )
}
